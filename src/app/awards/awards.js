angular.module( 'ngBoilerplate.awards', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'awards', {
    url: '/awards',
    views: {
      "main": {
        controller: 'awardsCtrl',
        templateUrl: 'awards/awards.tpl.html'
      }
    },
    data:{ pageTitle: 'What is It?' }
  });
})

.controller( 'awardsCtrl', function awardsCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

;
