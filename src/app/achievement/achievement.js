angular.module( 'ngBoilerplate.achievement', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'achievement', {
    url: '/achievement',
    views: {
      "main": {
        controller: 'achievementCtrl',
        templateUrl: 'achievement/achievement.tpl.html'
      }
    },
    data:{ pageTitle: 'What is It?' }
  });
})

.controller( 'achievementCtrl', function achievementCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

;
