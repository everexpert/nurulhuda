angular.module( 'ngBoilerplate.admin', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])



    .config(function config( $stateProvider ) {
        $stateProvider.state( 'admin', {
            url: '/admin',
            views: {
                "main": {
                    controller: 'AdminCtrl',
                    templateUrl: 'admin/admin.tpl.html'
                }
            },
            data:{ pageTitle: 'admin' }
        })
            .state( 'admin.poems', {
                url: '/poems',
                views: {
                    "adminview": {
                        controller: 'PoemsCtrl',
                        templateUrl: 'admin/view/poem/poems.tpl.html'
                    }
                },
                data:{ pageTitle: 'poems' }
            })
            .state( 'admin.newpoem', {
                url: '/newpoem',
                views: {
                    "adminview": {
                        controller: 'NewPoemCtrl',
                        templateUrl: 'admin/view/poem/new-poem.tpl.html'
                    }
                },
                data:{ pageTitle: 'newpoem' }
            })
            .state( 'admin.categories', {
                url: '/categories',
                views: {
                    "adminview": {
                        controller: 'CategoriesCtrl',
                        templateUrl: 'admin/view/poem/categories.tpl.html'
                    }
                },
                data:{ pageTitle: 'categories' }
            })
            .state( 'admin.newcategories', {
                url: '/newcategories',
                views: {
                    "adminview": {
                        controller: 'NewCategoriesCtrl',
                        templateUrl: 'admin/view/poem/new-categories.tpl.html'
                    }
                },
                data:{ pageTitle: 'newcategories' }
            });
    })


    .controller( 'AdminCtrl', function AdminCtrl( $scope) {

    })









    .controller( 'PoemsCtrl', function PoemsCtrl( $scope,$http,$rootScope){

//        $scope.gridOptions = {
//            enableSorting: true,
//            enableFiltering:true,
//            showTreeExpandNoChildren: true,
//            columnDefs: [
//                { name: 'Post Title', width: '25%' },
//                { name: 'Post Title Slug', width: '25%' },
//                { name: 'Post Desc', width: '25%' },
//                { name: 'Category Action', width: '25%' }
//            ]
//        };
////http://localhost/nurulhuda/server/test.php
//        $http({
//            method: 'GET',
//            url: $rootScope.baseurl+'/server/test.php'
//        }).then(function successCallback(response,data) {
//            // this callback will be called asynchronously
//            // when the response is available
//            console.log(response.data);
//            console.log(response);
//            $scope.result = response.data;
//        }, function errorCallback(response) {
//            // called asynchronously if an error occurs
//            // or server returns response with an error status.
//        });

    })
    .controller( 'NewPoemCtrl', function NewPoemCtrl( $scope){

        //$scope.gridOptions = {
        //    enableSorting: true,
        //    enableFiltering:true,
        //    showTreeExpandNoChildren: true,
        //    columnDefs: [
        //        { name: 'Post Title', width: '25%' },
        //        { name: 'Post Title Slug', width: '25%' },
        //        { name: 'Post Desc', width: '25%' },
        //        { name: 'Category Action', width: '25%' }
        //    ]
        //
        //};

    })











    .controller( 'CategoriesCtrl', function CategoriesCtrl( $scope,$rootScope,$http){
        $scope.gridOptions = {
            enableSorting: true,
            columnDefs: [
                { name:'id', field: 'id', enableCellEdit:false},
                { name:'title', field: 'title' },
                { name:'description', field: 'description' },
                { name:'isPublished', field: 'isPublished'}
            ]
        };
        var method = 'POST';
        var url = $rootScope.baseurl+'/server/poem/show-poem-categories.php';

        var FormData = {
            'limit' : 400,
            'start' : 1
        };
        $http({
            method: method,
            url: url,
            data: FormData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
        success(function(response) {
            console.log(response);
            $scope.gridOptions.data =response;

        }).
        error(function(response) {
            console.log(response);
        });

    })

    .controller( 'NewCategoriesCtrl', function NewCategoriesCtrl( $scope,$http,$rootScope){

        $scope.saveCategory = function(){
            console.log($scope.category);

            $http.post($rootScope.baseurl+'/server/poem/add-poem-category.php', $scope.category).then($scope.successCallback, $scope.errorCallback);
        };

        $scope.successCallback = function(response){
            console.log(response);
        };

        $scope.errorCallback = function(){
            console.log("error occured");
        };

    });


