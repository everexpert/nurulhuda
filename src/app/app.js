angular.module( 'ngBoilerplate', [
  'templates-app',
  'templates-common',
  'ngBoilerplate.home',
  'ngBoilerplate.about',
  'ngBoilerplate.achievement',
  'ngBoilerplate.publications',
  'ngBoilerplate.awards',
  'ngBoilerplate.gallery',
  'ngBoilerplate.contact',
  'ngBoilerplate.admin',
  'ngAnimate',
  'ngTouch',
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.treeView',
  'uiGmapgoogle-maps',
  'ui.router'

])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run () {

})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location,$rootScope ) {
      $rootScope.baseurl = "/nurulhuda";
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | ngBoilerplate' ;
    }
  });
})

;
