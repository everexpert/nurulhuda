angular.module( 'ngBoilerplate.publications', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'publications', {
    url: '/publications',
    views: {
      "main": {
        controller: 'PublicationsCtrl',
        templateUrl: 'publications/publications.tpl.html'
      }
    },
    data:{ pageTitle: 'publicationss' }
  })

  .state('publications.Books',{
      url:'/Books',
      views:{
        "publication":{
          controller:'bookCtrl',
          templateUrl:'publications/book_collection/Books.tpl.html'
        }
      },
      data:{ pageTitle: 'Books' }
  })
 .state('publications.EnglishBooks',{
      url:'/EnglishBooks',
      views:{
        "publication":{
          controller:'EnglishBooksCtrl',
          templateUrl:'publications/book_collection/EnglishBooks.tpl.html'
        }
      },
      data:{ pageTitle: 'Books' }
  })
  .state('publications.BanglaBooks',{
      url:'/BanglaBooks',
      views:{
        "publication":{
          controller:'BanglaBooksCtrl',
          templateUrl:'publications/book_collection/BanglaBooks.tpl.html'
        }
      },
      data:{ pageTitle: 'Books' }
  });

})















.controller( 'PublicationsCtrl', function AboutCtrl( $scope ) {



  // $scope.myInterval = 5000;
  // $scope.noWrapSlides = false;
  // var slides = $scope.slides = [];
  // $scope.addSlide = function() {
  //   slides.push({
  //       $scope:photos= [
  //       {image: 'assets/images/publication/Allimages/1.jpg'},
  //       {image: 'assets/images/publication/Allimages/1.jpg'},
  //       {image: 'assets/images/publication/Allimages/1.jpg'},
  //       {image: 'assets/images/publication/Allimages/1.jpg'}
  //     ]
  //   });

  // };
  // for (var i=0; i<1; i++) {
  //   $scope.addSlide();
  // }

  $(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
 
      items :7,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
 
  });
 
});

})





.controller('bookCtrl', function bookCtrl($scope) {

$scope.publicationsbooks=[
  {
    bookimg:'assets/images/publication/banglabooks/1.jpg',
    bheading:'Shat Bosorar Kobita',
    bdetails:'His poems have been translated into many international languages including English, French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/2.jpg',
    bheading:'Amar Sasostro Shobdowbahini',
    bdetails:' French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/3.jpg',
    bheading:'Shuvojatra Drabirar Proti',
    bdetails:' German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/4.jpg',
    bheading:'Jonmajati',
    bdetails:'His poems have been translated into many international languages including English, French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/5.jpg',
    bheading:'Sokuntola',
    bdetails:'His poems have been translated into many international languages including English, French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/6.jpg',
    bheading:'Honolul',
    bdetails:'His poems have been translated into many international languages including English, French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/7.jpg',
    bheading:'Kusumer Phona',
    bdetails:'His poems have been translated into many international languages including English, French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  },
  {
    bookimg:'assets/images/publication/banglabooks/8.jpg',
    bheading:'Baro Bosorar Golpho',
    bdetails:'His poems have been translated into many international languages including English, French, German, Russian, Arabic, Urdu, Hundi etc. He has won more than fifty awards at home and abroad including Bangla Academy Literary Award'
  }
];
})
.controller('EnglishBooksCtrl', function bookCtrl($scope) {



$scope.publicationsbooks=[
  {

  }

];
})
.controller('BanglaBooksCtrl', function bookCtrl($scope) {
  $scope.publicationsbooks=[
  {

  }

];
});