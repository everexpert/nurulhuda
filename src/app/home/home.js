/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 // */
angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'plusOne'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home',
    views: {
      "main": {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'HomeCtrl', function HomeController( $scope ) {

        $scope.activities=[
            {
                acimg:'assets/images/home/huda-vai-2.jpg',
                acheader:'মুহম্মদ নূরুল হুদার একক বইমেলা',
                acdarticle:'কবি মুহম্মদ নূরুল হুদার ৬৬তম জন্মবার্ষিকী উপলক্ষ্যে রাজধানীর পাঠক সমাবেশ কেন্দ্রে কবি মুহম্মদ নূরুল হুদার শতাধিক বইয়ের প্রদর্শনী এবং ত্রিশের অধিক বই নিয়ে আজ ৩০ সেপ্টেম্বর থেকে ৭ দিনব্যাপী বইমেলার আয়োজন করেছে বইনিউজ টোয়েন্টিফোর.কম। কবি মুহম্মদ নূরুল হুদার “বেহুলার শাড়ী“ কবিতা গ্রন্থের মোড়ক উন্মোচন করে অনুষ্ঠানটির উব্দোধন করেন সব্যসাচী লেখক সৈয়দ শামসুল',
                mjhdate:'30 sep, 2015'
            },
            {
                acimg:'assets/images/home/recent_a2.jpg',
                acheader:'Mohammad Nurul Huda',
                acdarticle:'Mohammad Nurul Huda (born on 30 September, 1949, Cox’s Bazar) is a leading Bengali poet of international repute in today’s Bangladesh with more than 100 titles published to his credit. His poetry books are above sixty in number including hisMohammad Nurul Huda (born on 30 September, 1949, Cox’s Bazar) is a leading Bengali poet of international repute.',
                mjhdate:'18 oct, 2015'
            },
            {
                acimg:'assets/images/home/recent_a3.jpg',
                acheader:'Mohammad Nurul Huda',
                acdarticle:'Mohammad Nurul Huda (born on 30 September, 1949, Cox’s Bazar) is a leading Bengali poet of international repute in today’s Bangladesh with more than 100 titles published to his credit. His poetry books are above sixty in number including hisMohammad Nurul Huda (born on 30 September, 1949, Cox’s Bazar) is a leading Bengali poet of international repute.',
                mjhdate:'18 oct, 2015'
            }

        ];


 //$scope.p_data=
 //[
 // {
 //   imagineimg:'assets/images/home/Photo-0068.jpg',
 //   header:'Mother Tongue',
 //   article:'Men are not rivers, yet in their hearts Burns the raging thirst of rivers.The youthful blood that was spilled in Fifty-two With its tidal thirst suddenly becomes A river of humanity across the Dravidian delta See how on its alluvial...',
 //   mjhLink:'publications.EnglishBooks'
 //   },
 //   {
 //    imagineimg:'assets/images/home/Photo-0068.jpg',
 //    header:'শব্দিত পতন',
 //   article:'অনন্তর অবয়বে শব্দিত পতন, শিরশির প্রজাপতি পাখার আওয়াজ,মন্দ্রিত ভেরীর ডাক ডুবে গেলো পুঞ্জ পুঞ্জ ধুমট মোড়কে, ভীষণ শব্দের পাঁকে ডুবে যাচ্ছি, আমি যেন ডুবে যাচ্ছি নিঃশ্বাসের ফাঁদে।  নিঃশ্বাসের ফাঁদে ফাঁদে ডুবে যাচ্ছি ডুবে যাচ্ছি ডুবে যাচ্ছি আমি - আমার  ...',
 //    mjhLink:'publications.BanglaBooks'
 // },
 //   {
 //   imagineimg:'assets/images/home/Photo-0068.jpg',
 //   header:'A Baby, Just Born Just Dead, Says',
 //    article:'Neither the call to prayer nor the conch-shell’s sound,on the morning of my birth I heard only murderous laughter of hundreds of thousands of...',
 //    mjhLink:'publications.EnglishBooks'
 //
 // },
 //   {
 //    imagineimg:'assets/images/home/Photo-0068.jpg',
 //   header:'ডানকানের মুখের মত',
 //   article:'সূর্যাস্তের উপত্যকা পার হয়ে অন্ধকার জ্বালালেই যুগল প্রদীপ   আকাশ তরল হয়ে দ্রবীভূত হয় কালো জলে, আর পাখির কুজন শুনে থমকে দাঁড়ালে খেয়াঘাটে এক পা এগিয়ে জনক আসেন তাঁর সপ্তরঙ পতাকা উড়িয়ে।  জ্বি, আজীবন নিগৃহীত ইনিই জনক,...',
 //   mjhLink:'publications.BanglaBooks'
 //  },
 //   {
 //    imagineimg:'assets/images/home/Photo-0068.jpg',
 //   header:'A Coppery Nation We are',
 //    article:'Bathed in the sun, grown up in the rains Of thousand centuries built we are We have come, a coppery nation we are.Approaching events to the life adhere At the moment of birth There sticks a remembrance of the gone-by past...',
 //     mjhLink:'publications.EnglishBooks'
 //  }
 //
 //];



});





