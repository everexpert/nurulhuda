    <?php

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $limit = $request->limit;
    $start = $request->start;

    $servername = "localhost";
    $username = "root";
    $password = "123";
    $dbname = "nurulhuda";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT id, title, description, isPublished FROM poem_category LIMIT ".$limit;
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $rows = [];
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        //print_r($rows);
        header('Content-Type: application/json');
        echo json_encode($rows);

    }else {
        echo "0 results";
    }
    $conn->close();